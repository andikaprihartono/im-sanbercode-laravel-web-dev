<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function home(){
        return view ('home');
    }

    public function register(){
        return view ('register');
    }

    public function send(Request $request){
        $firstName=$request['first-name'];
        $lastName = $request['last-name'];
        return view('welcome',['first'=>$firstName,'last'=>$lastName]);
    }
}
