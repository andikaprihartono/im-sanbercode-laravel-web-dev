<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sanbercode</title>
</head>
<body>
    <main>
        <h1>Buat Account Baru</h1>
        <h3>Sign Up Form</h3>
        <form action="/send" method="post">
            @csrf
            <div>
                <label for="first-name">First Name:</label><br>
                <input type="text" name="first-name">
            </div>
            <div>
                <label for="last-name">Last Name:</label><br>
                <input type="text" name="last-name">
            </div>
            <div>
                <label>Gender:</label><br>
                <input type="radio" id="man" name="gender" value="man">
                <label for="man">Man</label><br>
                <input type="radio" id="woman" name="gender" value="woman">
                <label for="woman">Woman</label><br>
                <input type="radio" id="other" name="gender" value="other">
                <label for="other">Other</label><br><br>
            </div>
            <div>
                <label for="nationalitys">Nationality</label>
                <select name="nationalitys" id="nationalitys">
                <option value="indonesia">Indonesia</option>
                <option value="japan">Japan</option>
                <option value="arab">Arab</option>
                <option value="cina">Cina</option>
                </select>
            </div>
            <div>
                <label for="">Language Spoken</label><br>
                <input type="checkbox" id="bahasaindonesia" name="bahasaindonesia" value="bahasaindonesia">
                <label for="bahasaindonesia"> Bahasa Indonesia</label><br>
                <input type="checkbox" id="english" name="english" value="english">
                <label for="english"> English</label><br>
                <input type="checkbox" id="arabic" name="arabic" value="arabic">
                <label for="arabic"> Arabic</label><br>
                <input type="checkbox" id="japanese" name="japanese" value="japanese">
                <label for="japanese">Japanese</label><br>
            </div>
            <div>
                <label for="biodata">Biodata</label><br>
                <textarea name="biodata" id="" cols="30" rows="10"></textarea>
            </div>
            <button type="submit">Sign Up</button>
        </form>
    </main>
</body>
</html>
