@extends('layout.master')
@section('page-title')
Tambah
@endsection
@section('title')
Tambah Section
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="varchar" class="form-control" name="nama"   value="{{$cast->nama}}">
    </div>
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="int" class="form-control" name="umur" value="{{$cast->umur}}">
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" class="form-control"  cols="30" rows="10" >{{$cast->bio}}</textarea>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
