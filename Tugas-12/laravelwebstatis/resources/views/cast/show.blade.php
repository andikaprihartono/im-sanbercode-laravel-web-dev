@extends('layout.master')
@section('page-title')
Cast
@endsection
@section('title')
Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah Cast</a>

<div class="table-responsive">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item )
            <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-secondary">Edit</a>
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">

                </form>
            </td>
            </tr>
            @empty
            <tr>
                <td>Tidak ada data</td>
            </tr>
            @endforelse
         <tr>


         </tr>
        </tbody>
      </table>
  </div>
@endsection
