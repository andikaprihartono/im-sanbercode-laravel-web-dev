@extends('layout.master')
@section('page-title')
Tambah
@endsection
@section('title')
Tambah Section
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form method="POST" action="/cast">
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="varchar" class="form-control" name="nama"  placeholder="Masukan Nama">
    </div>
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="int" class="form-control" name="umur" placeholder="Masukkan Usia">
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" class="form-control"  cols="30" rows="10" placeholder="Masukkan Bio"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
